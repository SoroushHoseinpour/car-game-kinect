﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public GameObject[] obstacles;
    public float interval;

    private float timeCount;

    private void Start()
    {
        StartCoroutine(GameControl());
    }

    private IEnumerator GameControl()
    {
        yield return new WaitForSeconds(interval / 2);
        while (true)
        {
            var position = new Vector3(Random.Range(transform.position.x - 5, transform.position.x + 5),
                transform.position.y, transform.position.z);
            Instantiate(obstacles[Random.Range(0, obstacles.Length)], position, Quaternion.identity);
            yield return new WaitForSeconds(interval);
        }
    }
}