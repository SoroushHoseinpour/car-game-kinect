﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class ObstacleController : MonoBehaviour
{
    public float speed;
    private Rigidbody _rb;

    private void Start()
    {
        _rb = GetComponent<Rigidbody>();
        _rb.useGravity = false;
        _rb.velocity = new Vector3(0, 0, speed);
    }


    private void OnBecameInvisible()
    {
        Destroy(gameObject, 0.5f);
    }
}